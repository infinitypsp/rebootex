TARGET = rebootex661
OBJS = src/crt0.o src/main.o src/btcnf.o src/keyseed.o src/nandscramble.o src/compatibility.o \
        src/btcnfpatch.o src/lfatpatch.o src/loadcorepatch.o src/libc.o src/loadcorestartpatch.o \
        src/utility.o src/psp_uart.o src/kprintf.o src/syscon.o src/sysreg.o

INCDIR = include
CFLAGS = -Os -G0 -Wall
CXXFLAGS = $(CFLAGS) -fno-exceptions -fno-rtti
ASFLAGS = $(CFLAGS)

LIBS =
LIBDIR = lib
LDFLAGS = -nostartfiles -T src/linkfile.l -nostdlib

PSPSDK=$(shell psp-config --pspsdk-path)
include src/build.mak
