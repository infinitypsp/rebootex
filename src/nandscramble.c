/*
Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 

*/

#include "nandscramble.h"

#include <pspmacro.h>

void applyNandScramble(int model)
{
    switch (model)
    {
        case PSP_MODEL_PHAT:
        {
            // patch nand decryption to use 6.31 scrambles
            _sw(0x3C04E370, 0x8860BE38); // 6.61
            _sw(0x34871A7B, 0x8860BE3C); // 6.61
            break;
        }
        
        case PSP_MODEL_SLIM:
        {        
            // patch nand decryption to use 6.31 scrambles
            _sw(0x3C04E370, 0x8860BEF8); // 6.61
            _sw(0x34871A7B, 0x8860BEFC); // 6.61
            break;
        }
        
        case PSP_MODEL_PSPGO:
        {
            // patch nand decryption to use 6.31 scrambles
            _sw(0x3C04E370, 0x8860BEF8); // 6.61
            _sw(0x34871A7B, 0x8860BEFC); // 6.61
            break;
        }
        
        case PSP_MODEL_BRITE:
        case PSP_MODEL_BRITE4G:
        case PSP_MODEL_BRITE7G:
        case PSP_MODEL_BRITE9G:
        default:
        {    
            // patch nand decryption to use 6.31 scrambles
            _sw(0x3C04E370, 0x8860BEF8); // 6.61
            _sw(0x34871A7B, 0x8860BEFC); // 6.61
            break;
        }
    }
}
