/*
Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

*/

#include "compatibility.h"
#include "lfatpatch.h"
#include "btcnfpatch.h"
#include "loadcorepatch.h"
#include "keyseed.h"
#include "nandscramble.h"
#include "utility.h"
#include "syscon.h"

unsigned int g_coreVersion = -1;

void DisplayColour(u32 val)
{
	int i;

	for (i = 0x44000000; i < 0x44200000; i += 4)
	{
		((u32 *)i)[0] = val;
	}
}

unsigned int getCoreVersion(void)
{
    return g_coreVersion;
}

#ifdef MS0_LOAD
int entry(SceKernelBootParam *reboot_param, SceLoadExecInternalParam *exec_param, int api, int initial_rnd, int is_compat, int bootloader_version, const char *infinityctrl, int size)
{
    setInfinityctrl(infinityctrl, size);
#else
int entry(SceKernelBootParam *reboot_param, SceLoadExecInternalParam *exec_param, int api, int initial_rnd, int is_compat, int bootloader_version)
{
#endif
    // taken from IPL SDK
    u32 ctrl = 0, is_recovery = 0;
    _pspSysconGetCtrl1(&ctrl);

    // positive
    ctrl = ~ctrl;

    if ((ctrl & SYSCON_CTRL_RTRG) == SYSCON_CTRL_RTRG)
    {
        is_recovery = 1;
    }

    // if home button is pressed, we want to boot into OFW
    if ((ctrl & SYSCON_CTRL_HOME) == SYSCON_CTRL_HOME)
    {
        setCompatibilityInformation(0, 0, 0);
    }
    else
    {
        setCompatibilityInformation(is_compat, reboot_param->psp_model, is_recovery);
    }

    // apply all needed patches
    applyLfatPatches(reboot_param->psp_model);
    applyBtcnfPatches(reboot_param->psp_model);
    applyLoadcorePatch(reboot_param->psp_model);

    // for our hybrid firmware we need to set keyseed and nand scrambles to 6.31
    applyKeyseed(reboot_param->psp_model);
    applyNandScramble(reboot_param->psp_model);

    g_coreVersion = bootloader_version;
    ClearCaches();

    // just return, let our caller decide on the next action
	return 0;
}
