/*
Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 

*/

#include "keyseed.h"

#include <pspmacro.h>

#include <string.h>

// bfc encryption key parts
const char g_661_1000_keys[] = 
{
    0x06, 0x71, 0x54, 0x2E, 0x8B, 0xC0, 0xDF, 0xFB, 
    0x08, 0xCC, 0x7F, 0x08, 0x4E, 0x33, 0x60, 0xAA,
};

const char g_661_2000_keys[] = 
{
    0x89, 0x93, 0x0D, 0x83, 0x71, 0x7F, 0x02, 0xF2, 
    0xC5, 0x0D, 0x06, 0x2D, 0x05, 0xA9, 0x78, 0xBA, 
    0x4D, 0x82, 0xC9, 0x26, 0xA5, 0xA7, 0x35, 0x33, 
    0x68, 0x63, 0xCD, 0xD5, 0x1D, 0xA1, 0x36, 0xD1,
};

const char g_661_3000_keys[] = 
{
    0x43, 0x0E, 0x99, 0xA2, 0x73, 0x57, 0x37, 0x31, 
    0xBF, 0x22, 0x9C, 0x5B, 0x69, 0x79, 0xB0, 0xEC, 
    0xF0, 0x7B, 0x99, 0xDF, 0x21, 0x54, 0x80, 0xFA, 
    0x97, 0x37, 0x2D, 0xEB, 0x02, 0x73, 0x32, 0x94, 
};

const char g_661_go_keys[] = 
{
    0xEA, 0xCC, 0x21, 0x4E, 0x5C, 0x1F, 0x64, 0x31, 
    0x8A, 0x12, 0x20, 0x12, 0x39, 0xD5, 0x2D, 0x19, 
    0x70, 0x3F, 0xC8, 0x28, 0xB1, 0x02, 0x75, 0x40, 
    0x73, 0x94, 0xBA, 0xF5, 0x2A, 0x12, 0x0E, 0xA8,
};

void applyKeyseed(int model)
{
    switch (model)
    {
        case PSP_MODEL_PHAT:
        {
            // copy key to 0xBFC00200 so system uses 6.61 keys instead of IPL provided keys (6.31)
            memcpy((void *)0xBFC00200, g_661_1000_keys, sizeof(g_661_1000_keys));
            break;
        }
        
        case PSP_MODEL_SLIM:
        {
            // copy key seed to 0xBFC00200 so system uses 6.61 keys instead of IPL provided keys (6.31)
            memcpy((void *)0xBFC00200, g_661_2000_keys, sizeof(g_661_2000_keys));
            break;
        }
        
        case PSP_MODEL_PSPGO:
        {
            // copy key seed to 0xBFC00200 so system uses 6.61 keys instead of IPL provided keys (6.31)
            memcpy((void *)0xBFC00200, g_661_go_keys, sizeof(g_661_go_keys));
            break;
        }
        
        case PSP_MODEL_BRITE:
        case PSP_MODEL_BRITE4G:
        case PSP_MODEL_BRITE7G:
        case PSP_MODEL_BRITE9G:
        default:
        {
            // copy key seed to 0xBFC00200 so system uses 6.61 keys instead of IPL provided keys (6.31)
            memcpy((void *)0xBFC00200, g_661_3000_keys, sizeof(g_661_3000_keys));
            break;
        }
    }
}
