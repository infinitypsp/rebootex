/*
Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

*/

#include "loadcorepatch.h"
#include "utility.h"

#include <pspmacro.h>

#include <string.h>

// loadcore signcheck decryption functions
int (* DecryptExecutable)(void *buffer, int size, int *outsize) = NULL;
int (* UnSigncheck)(void *buffer, int size) = NULL;

// loadcore patch functions
void InjectLoadCorePatch(void);
void InjectLoadCorePatch01g(void);
void RestoreExecuteLoadCore(SceSize args, void *argp);


u32 loadcore_addr = 0;
u32 g_loadcore_bootstart = 0;
u32 g_loadcore_bootstart_backup[2];

int DecryptExecutablePatched(void *header, int size, int *outsize)
{
	/* check for decryption tag */
	if (_lw((u32)header + 0x130) == 0xDAEEDAEE)
	{
		*outsize = _lw((u32)header + 0xB0);
		memmove(header, header + 0x150, *outsize);
        MAKE_CALL(loadcore_addr + 0x5970, (u32)DecryptExecutable);
        MAKE_CALL(loadcore_addr + 0x5994, (u32)UnSigncheck);
		return 0;
	}

	/* return the real decryption code */
    return DecryptExecutable(header, size, outsize);
}

int UnSigncheckPatched(void *buffer, int size)
{
	int i;
	/* loop through the signcheck */
	for (i = 0; i < 0x58; i++)
	{
		/* if byte is 0 then call the signcheck removal */
		if (((u8 *)buffer)[0xD4 + i])
		{
			/* remove signcheck */
			return UnSigncheck(buffer, size);
		}
	}

	return 0;
}

void LoadCoreModuleStart(SceSize args, void *argp)
{
	/* get text_addr by substituting from module_bootstart address */
	u32 text_addr = g_loadcore_bootstart - 0xAF8;
	loadcore_addr = text_addr;

    ClearCaches();

	/* assign our function pointers */
	DecryptExecutable = (void *)EXTRACT_J_ADDR(text_addr + 0x5970);
	UnSigncheck = (void *)EXTRACT_J_ADDR(text_addr + 0x5994);

	/* patch the calls to the decryption */
	MAKE_CALL(text_addr + 0x5970, DecryptExecutablePatched);

	/* patch calls to the unsigncheck routines */
	MAKE_CALL(text_addr + 0x5994, UnSigncheckPatched);

	// restore loadcore's bootstart
    memcpy((void *)g_loadcore_bootstart, g_loadcore_bootstart_backup, sizeof(g_loadcore_bootstart_backup));
    ClearCaches();

    void (* bootstart)(SceSize args, void *argp) = (void *)g_loadcore_bootstart;
    bootstart(args, argp);
}

void OnLoadCoreInjectionPatch(u32 bootstart)
{
    // backup loadcore
    g_loadcore_bootstart = bootstart;
    memcpy(g_loadcore_bootstart_backup, (void *)bootstart, sizeof(g_loadcore_bootstart_backup));

    // redirect bootstart to our function
    REDIRECT_FUNCTION(bootstart, LoadCoreModuleStart);
    ClearCaches();
}

void applyLoadcorePatch(int model)
{
    switch (model)
    {
        case PSP_MODEL_PHAT:
        {
            // overwrite module_bootstart address read
            _sw(0xAFB10004, 0x88605628); // 6.61
            _sw(0x8E510028, 0x88605630); // 6.61
            MAKE_JUMP(0x88605634, InjectLoadCorePatch01g); // 6.61
            break;
        }

        case PSP_MODEL_SLIM:
        {
            // hook loadcore start
            // overwrite module_bootstart address read
            _sw(0xAFB10004, 0x886056E8); // 6.61
            _sw(0x8E510028, 0x886056F0); // 6.61
            MAKE_JUMP(0x886056F4, InjectLoadCorePatch); // 6.61
            break;
        }

        case PSP_MODEL_PSPGO:
        {
            // hook loadcore start
            // overwrite module_bootstart address read
            _sw(0xAFB10004, 0x886056E8); // 6.61
            _sw(0x8E510028, 0x886056F0); // 6.61
            MAKE_JUMP(0x886056F4, InjectLoadCorePatch); // 6.61
            break;
        }

        case PSP_MODEL_BRITE:
        case PSP_MODEL_BRITE4G:
        case PSP_MODEL_BRITE7G:
        case PSP_MODEL_BRITE9G:
        default:
        {
            // hook loadcore start
            // overwrite module_bootstart address read
            _sw(0xAFB10004, 0x886056E8); // 6.61
            _sw(0x8E510028, 0x886056F0); // 6.61
            MAKE_JUMP(0x886056F4, InjectLoadCorePatch); // 6.61
        }
    }
}
